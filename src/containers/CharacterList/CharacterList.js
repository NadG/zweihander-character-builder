import React, {Component} from 'react';
import style from './../../sass/modules/character-index.module.scss'
import axios from 'axios'
import CharacterCardDetail from "../../components/CharacterCardDetail/CharacterCardDetail";
import CharacterSheet from "../CharacterSheet/CharacterSheet";
import {getFormattedSex} from './../../utils/dataDisplayMethods'
import {Route, Switch} from "react-router-dom";
import ModalForm from "../../components/Modals/ModalForm";
import styles from "../../sass/modules/character-index.module.scss";

class CharacterList extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    characterList: null,
    apiCallDone: false,
    characterSheetData: {
      rolledNewCharacterToken: null,
    },
    optionalRolls: ['roll-ancestry', 'roll-drawback', 'unlink-alignment'],
    showModalForm: false,
    isSelectedCharacter: false,
    isRolledNewClicked: false,
    optionalRollsSelected: {
      optionalRolls: []
    }
  }

  componentDidMount() {
    axios.get('/characters/').then(res => {
      this.setState({
        characterList: res.data,
        apiCallDone: true
      })
    }).catch(err => {
      console.log(err);
    });
  }

  renderModalForm() {
    this.setState({
      showModalForm: true,
      isScrollable: document.body.style.overflow = "hidden",
      modalHeight: window.screen.height + 'px'
    })
  }

  parseParamsForUrl(obj) {
    let parsedString = '';
    let acc;
    for (let key in obj) {
      acc = key + "=" + obj[key];
      parsedString = parsedString + acc + '&'
    }
    let urlParamsString = '?' + parsedString.substring(0, parsedString.length - 1)
    return urlParamsString;

    /* hai l'oggetto params
    quello che devi ottenere è una stringa che concateni chiavi e valori dell'oggetto con un ?
    es { a: 1, b: 2} => "a=1&b=2"
    Passi questa stringa alla history.push dopodichè in characterSheet  ti vai a leggere queryString.parse(this.props.location.search) e li usi per i tuoi scopi
    *
    * */
  }

  renderCharacterList() {
    const {characterList, apiCallDone} = this.state
    let list = [];
    if (apiCallDone) {
      if (characterList.length > 0) {
        list = characterList.map((e, i) => {
          return (
            <CharacterCardDetail
              name={e.name}
              ancestry={e.ancestry}
              buildType={e.buildType.name}
              sex={getFormattedSex(e.sex)}
              profession={e.profession}
              ageGroup={e.ageGroup}
              key={i}
              id={e.id}
              view={this.handleViewCharacterSelected}
            />
          )
        })
      } else {
        list = <p>There are any character saved yet. Roll a new one</p>
      }
    } else {
      list =
        <h4 className={style.FlexBox}>Loading<span className={[style.zwLoading, styles.zwIcon].join(' ')}/></h4>
    }
    return list
  }

  handleOptionalRollParamsSelection = (e) => {
    let checkedOptionValue = e.target.value;
    let index = this.state.optionalRollsSelected.optionalRolls.indexOf(checkedOptionValue);
    if (index > -1) {
      this.state.optionalRollsSelected.optionalRolls.splice(index, 1)
      this.setState({
        optionalRollsSelected: {
          optionalRolls: [...this.state.optionalRollsSelected.optionalRolls]
        },
      })
    } else {
      if (index === -1) {
        this.setState({
          optionalRollsSelected: {
            optionalRolls: [...this.state.optionalRollsSelected.optionalRolls, checkedOptionValue]
          }
        })
      }
    }
  }

  handleViewCharacterSelected = (characterSelectedID) => {
    this.setState({
      isSelectedCharacter: true
    })
    this.props.history.push({pathname: '/character-sheet/view-character/' + characterSelectedID})
  }

  handleRollNewCharacter = () => {
    const {optionalRolls} = this.state.optionalRollsSelected
    const params = optionalRolls.reduce((current, item) => {
      current[item] = true;
      return current
    }, {})
    console.log(params);

    let paramsString = this.parseParamsForUrl(params)

    this.setState({
      ...this.state,
      showModalForm: false,
      isRolledNewClicked: true,
      isScrollable: document.body.style.overflow = "scroll"
    });

    this.props.history.push({pathname: '/character-sheet/' + paramsString})
  }

  handleCloseModal = () => {
    this.setState({
      showModalForm: false,
      isScrollable: document.body.style.overflow = "scroll"
    })
  }

  render() {
    let modal = this.state.showModalForm ?
      <ModalForm
        title="Roll a new character"
        fieldType="checkbox"
        checkboxOptions={this.state.optionalRolls}
        checkboxesChecked={this.handleOptionalRollParamsSelection}
        discard={this.handleCloseModal}
        readyToRoll={this.handleRollNewCharacter}
        height={this.state.modalHeight}
      /> : null;

    return (
      <div
        className={this.state.showModalForm ? style.OverflowHidden : [style.PageSpacing, style.WrapperGrid].join(' ')}>
        {modal}
        <div style={{gridArea: 'component'}}>
          <div className={style.RollNewContainer}>
            <button className={style.zwRollBtn} onClick={() => this.renderModalForm()}>
              <h1>Roll new</h1>
            </button>
          </div>
          <div className={style.ContentSectionContainer}>
            <br/>
            <h1 className={style.PageTitle}>Characters saved</h1>
            <br/>
            <div className={style.CharacterListContainer}>
              {this.renderCharacterList()}
            </div>
          </div>
        </div>
        <Switch>
          <Route path={this.props.match.url + '/character-sheet/:options?'}
                 component={() => <CharacterSheet/>}/>
          <Route path={this.props.match.url + '/character-sheet/view-character/:id'}
                 render={({history}) => <CharacterSheet history={history}/>}/>
        </Switch>
      </div>
    )
  }
}

export default CharacterList;
