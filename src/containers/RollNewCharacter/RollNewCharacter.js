import React, {Component} from 'react';
import styles from './../../sass/modules/character-index.module.scss'
import axios from 'axios'
import Checkbox from "../../components/Checkbox/Checkbox";

class RollNewCharacter extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    rolledNewCharacterToken: null,
    optionalRolls: ['roll-ancestry', 'roll-drawback', 'unlink-alignment'],
    optionalRollsSelected: {
      optionalRolls: []
    }
  }

  handleRollNewCharacter = () => {
    const {optionalRolls} = this.state.optionalRollsSelected
    const params = optionalRolls.reduce((current, item) => {
      current[item] = true;
      return current
    }, {})
    axios.get('/characters/roll-new', {params: params}).then(res => {
      console.log(res.data);
      this.setState({
        ...this.state,
        characterSheetData: res.data,
        rolledNewCharacterToken: res.headers['x-character-token']
      })
    })
  }

  handleOptionalRollParamsSelection = (e) => {
    let checkedOptionValue = e.target.value;
    let index = this.state.optionalRollsSelected.optionalRolls.indexOf(checkedOptionValue);
    if (index > -1) {
      this.state.optionalRollsSelected.optionalRolls.splice(index, 1)
      this.setState({
        optionalRollsSelected: {
          optionalRolls: [...this.state.optionalRollsSelected.optionalRolls]
        },
      })
    } else {
      if (index === -1) {
        this.setState({
          optionalRollsSelected: {
            optionalRolls: [...this.state.optionalRollsSelected.optionalRolls, checkedOptionValue]
          }
        })
      }
    }
  }

  render() {
    return (
      <div className={styles.FlexBoxRow} style={{'alignItems': 'center'}}>
        <Checkbox
          options={this.state.optionalRolls}
          change={this.handleOptionalRollParamsSelection}/>
        <button className={styles.zwButtonPrimary} onClick={() => this.handleRollNewCharacter()}>
          <h1>Roll new</h1>
          <span className={[styles.zwDice, styles.zwIcon].join(' ')}/>
        </button>
      </div>
    )
  }
}

export default RollNewCharacter;
