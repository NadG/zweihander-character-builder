import React, {Component} from 'react';
import utils from './../../sass/utils/utils-main.module.scss'
import {Route, Switch} from "react-router-dom";

class Header extends Component {
  constructor(props) {
    super(props);
    console.log(this.props);
  }

  returnHome = () => {
    this.props.history.push({pathname: '/'})
  }

  render() {
    return (
      <header className={[utils.Logo, utils.HeaderBottomBorder].join(' ')} onClick={() => this.returnHome()}>
        <h1>Zweihander</h1>
      </header>
    )
  }
}

export default Header;
