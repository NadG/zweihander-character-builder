import React, {Component} from 'react';
import axios from 'axios'
import {
  getPrimaryAttributes,
  getPrimaryAttributesBonuses,
  getUpbringing,
  getCharacterAppearance,
  getGeneralData,
  getAlignment,
  getGeneralPoints
} from '../../utils/utils'
import {getFormattedText} from '../../utils/dataDisplayMethods'
import PointSections from "./PointsSections/PointSections";
import CharacterGeneralData from "./CharacterGeneralData/CharacterGeneralData";
import CharacterAppearance from "./CharacterAppearance/CharacterAppearance";
import styles from "../../sass/modules/character-index.module.scss";
import style from '../../sass/modules/character-card.module.scss';
import * as queryString from "query-string";
import ModalForm from "../../components/Modals/ModalForm";
import DescriptiveTrait from "../../components/DescriptiveTrait/DescriptionSection";

class CharacterSheet extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    characterSheetData: null,
    isSheetExistent: null,
    showModal: false
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    // here the queryStrng is needed because I'm trying to pass an object through the routes
    const params = queryString.parse(this.props.match.params.options);
    if (id) {
      axios.get(`/characters/${id}`)
        .then(res => {
          this.setState({
            characterSheetData: res.data,
            isSheetExitent: true
          });
        })
    } else {
      axios.get('/characters/roll-new', {params: params}).then(res => {
        this.setState({
          ...this.state,
          characterSheetData: res.data,
          isSheetExistent: false,
          rolledNewCharacterToken: res.headers['x-character-token']
        })
      })
    }
  }

  renderPrimaryAttributes() {
    let points;
    if (this.state.characterSheetData) {
      points = <PointSections
        points={getPrimaryAttributes(this.state.characterSheetData)}
        bonuses={getPrimaryAttributesBonuses(this.state.characterSheetData)}
        upbringing={getUpbringing(this.state.characterSheetData)}
        border/>
    } else {
      points = <p>Loading...</p>
    }
    return points
  }

  renderGeneralPoints() {
    let points;
    if (this.state.characterSheetData) {
      points = <PointSections points={getGeneralPoints(this.state.characterSheetData)}/>
    } else {
      points = <p>Loading...</p>
    }
    return points
  }

  renderAlignment() {
    let alignment;
    if (this.state.characterSheetData) {
      alignment = <PointSections points={getAlignment(this.state.characterSheetData)} notInGrid forAlignment/>
    } else {
      alignment = <p>Loading...</p>
    }
    return alignment
  }

  renderGeneralData() {
    let generalData;
    if (this.state.characterSheetData) {
      generalData = <CharacterGeneralData data={getGeneralData(this.state.characterSheetData)}
                                          upbringing={getUpbringing(this.state.characterSheetData)}/>
    } else {
      generalData = <p>Loading...</p>
    }
    return generalData
  }

  renderDescriptiveTraits() {
    const {characterSheetData} = this.state;
    let descriptiveTrait;
    if (characterSheetData) {
      descriptiveTrait = (
        <div>
          <br/>
          <DescriptiveTrait traitType="Ancestral trait" traitName={characterSheetData.ancestralTrait.name}
                            traitEffect={characterSheetData.ancestralTrait.effect}/>
          <br/>
          <DescriptiveTrait traitType="Dooming" traitEffect={characterSheetData.dooming}/>
          <br/>
          {characterSheetData.drawback ?
            <DescriptiveTrait traitType="Drawback" traitName={characterSheetData.drawback.name}
                              traitEffect={characterSheetData.drawback.effect}/> : null}
        </div>
      )
    } else {
      descriptiveTrait = <p>Loading...</p>
    }
    return descriptiveTrait
  }

  renderAppearance() {
    let appearance;
    if (this.state.characterSheetData) {
      appearance = <CharacterAppearance data={getCharacterAppearance(this.state.characterSheetData)}/>
    } else {
      appearance = <p>Loading...</p>
    }
    return appearance
  }

  renderTrappings() {
    let trappings;
    if (this.state.characterSheetData) {
      trappings = (
        <>
          <h4 style={{color: '#c90606'}}>Trappings</h4>
          <br/>
          {this.state.characterSheetData.trappings.map((e, i) => <p key={i}>&bull; {getFormattedText(e)}</p>)}
        </>
      )
    } else {
      trappings = <p>Loading...</p>
    }
    return trappings
  }

  renderModalForm() {
    this.setState({
      showModal: true,
      isScrollable: document.body.style.overflow = "hidden",
      modalHeight: window.screen.height + 'px'
    })
  }

  handleSaveCharacter = () => {
    this.setState({
      showModal: false,
      isScrollable: document.body.style.overflow = "scroll",
    })
    const headers = {
      'content-type': 'application/json',
      'x-character-token': this.state.rolledNewCharacterToken
    }
    axios.post('/characters/save', {name: this.state.nameChosen}, {headers: headers})
      .then(res => {
        this.props.history.push({pathname: '/'})
      })
      .catch(err => {
        console.log(err);
      })
  }

  handleDiscardCharacter = () => {
    this.setState({
      showModal: false,
      isScrollable: document.body.style.overflow = "scroll",
    })
    this.props.history.push({pathname: '/'})
  }

  handleInputChange = (e) => {
    this.setState({
      nameChosen: e.target.value
    });
    e.preventDefault();
  }

  render() {
    let inputName = this.state.showModal ? <ModalForm
      title="Give a name to this character"
      fieldType="input"
      inputChange={this.handleInputChange}
      discard={this.handleDiscardCharacter}
      saveCharacter={this.handleSaveCharacter}
      height={this.state.modalHeight}
    /> : null;

    let buttonsToRender;
    if (!this.state.isSheetExitent) {
      buttonsToRender = (
        <div className={style.FlexBoxRowBetween}>
          <button className={[styles.zwButtonSecondary, style.zwButton].join(' ')}
                  onClick={() => this.handleDiscardCharacter()}>
            Discard <span className={[style.zwCancel, styles.zwIcon].join(' ')}/>
          </button>
          <button className={[styles.zwButtonSecondary, style.zwButton].join(' ')}
                  onClick={() => this.renderModalForm()}>
            Save <span className={[style.zwSave, styles.zwIcon].join(' ')}/>
          </button>
        </div>
      )
    } else {
      buttonsToRender = (
        <button className={[styles.zwButtonSecondary, style.zwButton].join(' ')}
                onClick={() => this.handleDiscardCharacter()}>
          <span className={[style.zwBack, styles.zwIcon].join(' ')} style={{marginRight: '10px'}}/> Go back
        </button>
      )
    }

    return (
      <div
        className={this.state.showModal ? style.OverflowHidden : [style.CharacterSheetGrid, style.PageSpacing].join(' ')}>
        {inputName}
        <div className={style.ContentSectionContainer}>
          <div className={style.SheetHeaderAlignment}>
            {this.renderGeneralData()}
            {this.renderAlignment()}
          </div>
          <br/>
          {this.renderPrimaryAttributes()}
          <br/>
          <br/>
          {this.renderGeneralPoints()}
          <br/>
          <br/>
          <div className={style.FlexBox} style={{justifyContent: 'center'}}>
            <span className={[style.zwIcon, style.zwBorder3].join(' ')}/>
          </div>
          <br/>
          {this.renderDescriptiveTraits()}
          <br/>
          <div style={{textAlign: 'center'}}>
            {this.renderTrappings()}
          </div>
          <br/>
          <div style={{textAlign: 'center'}}>
            {this.renderAppearance()}
          </div>
          <br/>
          <br/>
          {buttonsToRender}
        </div>
      </div>
    )
  }
}

export default CharacterSheet;
