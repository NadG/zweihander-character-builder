import React from 'react'
import style from '../../../sass/utils/utils-main.module.scss';
import DescriptiveTrait from "../../../components/DescriptiveTrait/DescriptionSection";
import {getFormattedSex} from './../../../utils/dataDisplayMethods'

const CharacterGeneralData = (props) => {
  const {data} = props;
  let sex = getFormattedSex(data.sex);
  let name = data.name ? data.name : 'Name Lastname';

  return (
    <div>
      <div className={style.FlexBox}>
        <span>
          <h1>{name}</h1>
          <h4 className={style.SecondaryText} style={{'margin': '10px 0 10px 0'}}> {data.ancestry}</h4>
        </span>
      </div>
      <p style={{
        fontWeight: '700',
        marginTop: '10px',
        fontSize: '1.2rem'
      }}>{sex}, {data.ageGroup.toLowerCase()}, {data.socialClass.toLowerCase()}, {data.profession.toLowerCase()} </p>
      <br/>
      <ul className={style.DottedList}>
        <li>
          <p className={style.DescriptionText}>Born in <span>{data.seasonOfBirth}</span></p>
        </li>
        <li>
          <p className={style.DescriptionText}>Upbringing: <span>{data.upbringing.name}</span></p>
        </li>
        <li>
          <p className={style.DescriptionText}>Favoured primary
            attribute: <span>{data.upbringing.favoredPrimaryAttribute}</span></p>
        </li>
      </ul>
      <br/>
    </div>
  )
}

export default CharacterGeneralData;
