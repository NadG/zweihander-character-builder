import React from 'react'
import style from "./../../../sass/utils/utils-main.module.scss";

const CharacterAppearance = (props) => {
  let data = props.data;
  let buildType = props.data.buildType;
  let distinguishingMarksProperty;
  let distinguishingMarksTitle;

  if (data.distinguishingMarks) {
    distinguishingMarksTitle =
      <p style={{'fontWeight': 'bold'}} className={style.TitleSpacing}>Distinguishing marks: </p>
    distinguishingMarksProperty = data.distinguishingMarks.map((e, i) => {
      return (
        <ul>
          <li key={i}>
            <span className={[style.DescriptionText, style.TextCenter].join(' ')}>{e}</span>
          </li>
        </ul>
      )
    })
  }

  return (
    <div>
      <h3 className={style.TitleSpacing}>Appearance</h3>
      <p className={[style.DescriptionText, style.TextCenter].join(' ')}>
        <span>{data.eyeColor}</span> eyes,
        <span> {data.hairColor}</span> hairs,
        <span> {data.complexion}</span>,
        height of <span> {data.height}</span>,
        weight of <span> {data.weight}</span>,
        <span> {buildType.name.toLowerCase()} </span> ({buildType.priceModifier * 100}% on prices)
      </p>
      <br/>
      {distinguishingMarksTitle}
      {distinguishingMarksProperty}
    </div>
  )
}

export default CharacterAppearance;
