import React from 'react'
import style from '../../../sass/modules/character-card.module.scss';
import box from '../../../sass/modules/data-box.module.scss';
import DataBox from "../../../components/DataBox";
import {getFormattedText} from './../../../utils/dataDisplayMethods'

const PointSections = (props) => {

  const points = props.points;
  const bonuses = props.bonuses;
  const forAlignment = props.forAlignment;
  const upbringing = props.upbringing ? props.upbringing : null
  let pointsArray = []

  for (const [key, value] of Object.entries(points)) {
    if (value !== null) {
      let a;
      a = {
        title: key,
        points: value,
      }
      // if there are bonuses
      let newkey = a.title + "Bonus";
      if (bonuses && newkey in bonuses) {
        a.bonus = bonuses[newkey]
      }
      pointsArray.push(a)
    }
  }

  let pointBoxes = pointsArray.map((e, i) => {
    let favoredPA;
    let bonus = <p>bonus: <span>{e.bonus}</span></p>

    if (upbringing !== null) {
      favoredPA = upbringing.upbringing.favoredPrimaryAttribute.toLowerCase();
      return (
        <div className={favoredPA.trim() === e.title.trim() ? style.BorderedAccentBox : style.BorderedBox} key={i}>
          <DataBox title={getFormattedText(e.title)} value={e.points} bonus={e.bonus ? bonus : null}/>
        </div>
      )
    } else {
      return <DataBox title={getFormattedText(e.title)} value={e.points} bonus={e.bonus ? bonus : null} key={i}/>
    }
  })

  return (
    <div
      className={props.notInGrid ? [box.FlexBoxRowBetweenCenter, style.FluidContainer].join(' ') : box.PointBoxContainer}>
      {pointBoxes}
    </div>
  )
}

export default PointSections;
