import React, {Component} from 'react';
import CharacterList from "../CharacterList/CharacterList";
import {Switch, Route} from "react-router-dom";
import CharacterSheet from "../CharacterSheet/CharacterSheet";

class Home extends Component {
  render() {
    return (
      <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', height: '100%'}}>
        <Switch>
          <Route exact path="/" component={CharacterList}/>
          <Route path="/character-sheet/view-character/:id?" component={CharacterSheet}/>
          <Route path="/character-sheet/:options?" component={CharacterSheet}/>
        </Switch>
      </div>
    )
  }
}

export default Home;
