import React from 'react';
import input from './InputFile.module.scss'

const InputField = props => {
  return (
    <div>
      <input
        className={input.Input}
        value={props.value}
        type="text"
        onChange={props.clicked}
        autoFocus
      />
    </div>

  )
}

export default InputField
