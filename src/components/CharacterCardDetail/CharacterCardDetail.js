import React from 'react'
import charList from "../../sass/modules/character-index.module.scss";

const CharacterCardDetail = (props) => {
  return (
    <div className={charList.CharacterListItem}>
      <header>
        <h3>{props.name}</h3>
        <h4> {props.ancestry}</h4>
        <br/>
        <p>{props.sex}, {props.buildType}, {props.profession} </p>
      </header>
      <div className={charList.FlexBoxRow}>
        <button className={charList.zwButtonSecondary}>Delete</button>
        <button className={charList.zwButtonSecondary} onClick={() => props.view(props.id)}>View</button>
      </div>
    </div>
  )
}

export default CharacterCardDetail;
