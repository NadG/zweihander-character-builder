import React from 'react';
import modal from './Modal.module.scss'
import InputField from './../InputField/InputField'
import Checkbox from "../Checkbox/Checkbox";
import styles from "../../sass/modules/character-index.module.scss";

const ModalForm = props => {
  let field, buttonsToRender;
  switch (props.fieldType) {
    case 'input':
      field = <InputField label="Name" clicked={props.inputChange} value={props.nameTyped}/>
      buttonsToRender = <button className={styles.zwButtonSecondary} onClick={props.saveCharacter}>Save</button>
      break;
    case 'checkbox':
      field = <Checkbox options={props.checkboxOptions} change={props.checkboxesChecked}/>
      buttonsToRender = <button className={styles.zwButtonSecondary} onClick={props.readyToRoll}>Roll</button>
      break;
  }

  return (
    <div className={modal.Modal} style={{height: props.height}}>
      <h3>{props.title}</h3>
      <br/>
      {field}
      <br/>
      <div className={styles.FlexBoxRow}>
        <button className={styles.zwButtonSecondary} onClick={props.discard}>Discard</button>
        {buttonsToRender}
      </div>
    </div>
  )
}

export default ModalForm
