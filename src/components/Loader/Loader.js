import React from 'react'
import style from "../../sass/utils/utils-main.module.scss";

const Loader = () => {

    return (
        <div className={style.zwLoader}>
            <div></div>
            <div></div>
            <div></div>
        </div>
    )
}

export default Loader;
