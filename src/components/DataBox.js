import React from 'react'
import box from "../sass/modules/data-box.module.scss";
import utils from "../sass/utils/utils-main.module.scss";

const DataBox = (props) => {

    return (
        <div className={box.PointBox}>
            <h4 className={utils.TitleSpacing}>{props.title}</h4>
            <h2 className={utils.DataAccentTextSize}>{props.value}</h2>
            {props.bonus}
        </div>
    )
}

export default DataBox;
