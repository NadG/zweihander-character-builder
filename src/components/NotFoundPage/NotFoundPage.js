import React from 'react'
import styles from "../../sass/modules/character-index.module.scss";

const NotFoundPage = () => {
  return (
    <div className={styles.FlexBoxColumn}>
      <h1>404</h1>
      <p>The page doesn't exist</p>
    </div>
  )
}

export default NotFoundPage;
