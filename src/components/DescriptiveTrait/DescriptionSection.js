import React from 'react'
import style from "../../sass/utils/utils-main.module.scss";

const DescriptiveTrait = (props) => {
  let traitName = props.traitName ? <span>- {props.traitName}</span> : null;

  return (
    <div style={{
      'display': 'flex',
      'flexDirection': 'column',
      'justifyContent': 'center',
      'width': '100%',
      'alignItems': 'center'
    }}>
      <h3 className={style.TitleSpacing}>{props.traitType} {traitName}</h3>
      <p className={[style.DescriptionText, style.TextCenter].join(' ')}>{props.traitEffect}</p>
    </div>
  )
}

export default DescriptiveTrait;
