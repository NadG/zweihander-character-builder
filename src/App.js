import React from 'react';
import './App.css';
import {BrowserRouter} from 'react-router-dom'
import Home from "./containers/Home/Home";
import Header from "./containers/Header/Header";
import utils from "./sass/utils/utils-main.module.scss";
import style from "./sass/modules/character-index.module.scss";

function App() {
    return (
        <BrowserRouter>
            <Header/>
            <Home/>
            <footer className={utils.CreatorsFooter}>
                <div className={style.FlexBox} style={{justifyContent: 'center'}}>
                    <span className={[style.zwIcon, style.zwBorder2].join(' ')}/>
                </div>
                <br/>
                <span>
                    Coded with ❤  by <a href="https://www.linkedin.com/in/nadia-guarracino17/" target="_blank"> Nadia Guarracino </a>
                and <a href="https://www.linkedin.com/in/michele-sangalli-75b91a7a/"
                       target="_blank"> Michele Sangalli </a>
                    <br/>
                    Thanks to <a href="https://www.freepik.com/free-photos-vectors/background"> www.freepik.com</a> for icons and frames
                </span>
            </footer>
        </BrowserRouter>
    );
}

export default App;
