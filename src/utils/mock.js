export const DATA = {
    ageGroup: "Middle aged",
    agility: 32,
    agilityBonus: 2,
    ancestralTrait: {name: "Dauntless", effect: "You are immune to the effects of the Intimidate Skill and cannot be Stunned or Knocked Out!"},
    ancestry: "Elf",
    armor: "Leather",
    brawn: 38,
    brawnBonus: 3,
    buildType: {name: "Husky", priceModifier: 0.1},
    chaosAlignment: "Heresy",
    combat: 41,
    combatBonus: 5,
    complexion: "Light skin",
    damageThreshold: 5,
    distinguishingMarks:  ["Excessive body hair", "Patch of white hair"],
    dooming: "Do not push for more, as you will get it",
    drawback: null,
    encumbranceLimit: 6,
    eyeColor: "Pale green",
    fatePoints: 1,
    fellowship: 34,
    fellowshipBonus: 2,
    hairColor: "Ash blonde",
    height: "5.3ft",
    initiative: 8,
    intelligence: 47,
    intelligenceBonus: 5,
    movement: 5,
    name: null,
    orderAlignment: "Impiety",
    perception: 43,
    perceptionBonus: 5,
    perilThreshold: 6,
    profession: "Berserker",
    seasonOfBirth: "Autumn",
    sex: "f",
    socialClass: "Lowborn",
    trappings: ["fire-hardened spear", "heavy boots", "lantern", "laudanum (3)", "military attire", "oil pot", "red cap", "mushrooms", "rucksack", "suit of leather" ,"armor", "tincture (3)", "wooden shield"," arbalest crossbow with bolts (9) and quiver or mortuary sword or pike"],
    upbringing: {name: "Scholastic", favoredPrimaryAttribute: "Intelligence"},
    weight: "164lb",
    willpower: 40,
    willpowerBonus: 3
}

export const LIST =   [
    {
        "id":1,
        "name":"Ticsia Anzyana",
        "buildType":{
            "name":"Husky"
        },
        "sex":"f",
        "ageGroup":"Elderly",
        "ancestry":"Human",
        "profession":"Racketeer"
    },
    {
        "id":2,
        "name":"Tizyo Akaso",
        "buildType":{
            "name":"Husky"
        },
        "sex":"m",
        "ageGroup":"Young",
        "ancestry":"Human",
        "profession":"Pugilist"
    },
    {
        "id":3,
        "name":"Onna Baldrak",
        "buildType":{
            "name":"Normal"
        },
        "sex":"f",
        "ageGroup":"Adult",
        "ancestry":"Gnome",
        "profession":"Prostitute"
    },
    {
        "id":4,
        "name":"Flefa Poraccya",
        "buildType":{
            "name":"Corpulent"
        },
        "sex":"f",
        "ageGroup":"Middle aged",
        "ancestry":"Elf",
        "profession":"Monk"
    },
    {
        "id":5,
        "name":"Olo Gig",
        "buildType":{
            "name":"Husky"
        },
        "sex":"m",
        "ageGroup":"Elderly",
        "ancestry":"Human",
        "profession":"Prostitute"
    },
    {
        "id":6,
        "name":"Babushka",
        "buildType":{
            "name":"Normal"
        },
        "sex":"f",
        "ageGroup":"Elderly",
        "ancestry":"Human",
        "profession":"Apothecary"
    },
    {
        "id":7,
        "name":"Vacs Anty",
        "buildType":{
            "name":"Corpulent"
        },
        "sex":"m",
        "ageGroup":"Middle aged",
        "ancestry":"Human",
        "profession":"Old Believer"
    }
]
